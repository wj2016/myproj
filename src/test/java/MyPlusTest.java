import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MyPlusTest {
    @BeforeClass
    public void setUp() {
        System.out.println("Setup... BeforeClass");
    }

    @Test
    public void test_case_1() {
        System.out.println("======test case 1======");
        Assert.assertEquals(MyPlus.plus(1, 2), 3);
    }

    @Test
    public void test_case_2() {
        System.out.println("======test case 2======");
        Assert.assertEquals(MyPlus.plus(2, 2), 4);
    }

    @AfterClass
    public void tearDown() {
        System.out.println("Tear down...AfterClass");
    }
}
